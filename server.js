const express = require('express');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const sequelize = require('./database/connection');
const cookieParser = require('cookie-parser');  // ADD THIS

const indexRoutes = require('./routes/index');
const paymentRoutes = require('./routes/payment');

const app = express();
const PORT = process.env.PORT || 3000;

// Setting up EJS as template engine
app.set('view engine', 'ejs');

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser()); // ADD THIS BEFORE i18n.init
app.use('/static', express.static('public'));

// Translation
const i18n = require('i18n');

i18n.configure({
    locales: ['en', 'fr', 'ar'],
    directory: __dirname + '/locales',
    defaultLocale: 'en',
    cookie: 'lang',
    autoReload: true,
    updateFiles: false,
    syncFiles: false
});

app.use(i18n.init);

// Middleware for language switching
app.use('/lang/:lang', (req, res, next) => {
    res.cookie('lang', req.params.lang, { maxAge: 900000, httpOnly: true });
    res.redirect('back');
});

// Routes
app.use('/', indexRoutes);
app.use('/payment', paymentRoutes);

const pixelRoutes = require('./routes/pixel');
app.use(pixelRoutes);

// Start the server
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
    sequelize.authenticate()
        .then(() => {
            console.log('Database connected');
        })
        .catch(err => {
            console.error('Unable to connect to the database:', err);
        });
});
