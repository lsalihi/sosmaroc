const { Model, DataTypes } = require('sequelize');
const sequelize = require('../database/connection'); // Point to your sequelize config

class Pixel extends Model {}

Pixel.init({
    x_coord: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    y_coord: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    owner: {
        type: DataTypes.STRING,
        allowNull: false
    },
    sold: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
    }
}, {
    sequelize,
    modelName: 'pixel',
    tableName: 'pixels',
    timestamps: false
});

module.exports = Pixel;
