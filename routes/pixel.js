// Dependencies
const express = require('express');
const router = express.Router();
const NodeCache = require('node-cache');
const fs = require('fs');

// Node Cache instance
const cache = new NodeCache({ stdTTL: 3600 });

// Constants
const PARTITIONS = 100;

// Models
const Pixel = require('../models/Pixel');

const GRID_SIZE = 1000;

const GRID_KEY = "static_grid";
const GRID_INITED = "init_grid";

const getOrInitializeGrid = async () => {
    let grid = cache.get(GRID_KEY);
    cache.set(GRID_INITED, 'false');
    if (!grid) {
        grid = [];
        for (let x = 1; x <= GRID_SIZE; x++) {
            for (let y = 1; y <= GRID_SIZE; y++) {
                grid.push({
                    x_coord: x,
                    y_coord: y,
                    owner: 'SYSTEM',
                    sold: false
                });
            }
        }
        cache.set(GRID_KEY, grid);
        cache.set(GRID_INITED, 'true');
    }
    // It's important to return a deep copy of the grid, 
    // so that modifications to one grid don't affect the cached version.
    return JSON.parse(JSON.stringify(grid));
}

const updatePixelAsSold = (partitionPixels, x, y, ownerName) => {
    // This finds the specific pixel in the grid and updates it.
    let pixel = partitionPixels.find(p => p.x_coord === x && p.y_coord === y);
    if (pixel) {
        pixel.sold = true;
        pixel.owner = ownerName;
    }
}

router.get('/api/pixels', async (req, res) => {
    try {
        // Setup headers for SSE
        res.setHeader('Content-Type', 'text/event-stream');
        res.setHeader('Cache-Control', 'no-cache');
        res.setHeader('Connection', 'keep-alive');
        res.flushHeaders(); // Important for starting the streaming process

        // Create a deep copy of the static grid to avoid mutating the original grid
       /* getOrInitializeGrid().then(grid => {
            fs.writeFileSync('grid.json', JSON.stringify(grid, null, 2));
            
            console.log('Grid saved to grid.json');
            throw new Error("Grid saved to grid.json!");

        }).catch(error => {
            console.error('Error generating grid:', error);
        }); */






        for (let i = 1; i <= PARTITIONS; i++) {
            //throw new Error("pixel grid saved to pixel-grid.html!");
            // Fetch sold pixels from the database
           // const pixels = await Pixel.sequelize.query(`SELECT * FROM  pixels_part_${i}`, { model: Pixel, mapToModel: true });
            //const pixels = await Pixel.sequelize.query(`SELECT * FROM  p${i}`, { model: Pixel, mapToModel: true });
            const pixels = await Pixel.sequelize.query(
                `SELECT * FROM pixels PARTITION (p${i})`,
                { model: Pixel, mapToModel: true }
            );
            // Update those pixels in our copy of the static grid
            /*pixels.forEach(pixel => {
               // updatePixelAsSold(partitionPixels, pixel.x_coord, pixel.y_coord, pixel.owner);
            });*/

            // Send data to the client
            res.write(`data: ${JSON.stringify(pixels)}\n\n`);
        }
        console.log(cache.get(GRID_INITED) === true);
        console.log(cache.get(GRID_INITED));
        if(cache.get(GRID_INITED) === true) {
           // res.write(`data: ${JSON.stringify(partitionPixels)}\n\n`);
        }
        // If you want to close the connection after sending all the data:
        res.end();

    } catch (error) {
        console.error('Error fetching pixels:', error);
        
        // If an error occurs, send a relevant message
        res.write('data: {"error": "Failed to fetch pixels."}\n\n');
        res.end();
    }
});

module.exports = router;
