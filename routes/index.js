const express = require('express');
const Pixel = require('../models/Pixel');
const NodeCache = require('node-cache');
const router = express.Router();
// Create a new cache instance with a time-to-live of 1 hour (adjust as needed)
const pixelCache = new NodeCache({ stdTTL: 3600 });

// Route for the homepage
router.get('/', async (req, res) => {
    try {
        let lang = req.cookies.lang || 'en';

        res.render('index', {
            title: req.__('title'),
            description: req.__('description'),
            flagTooltip: req.__('flagTooltip'),
            supportTitle: req.__('supportTitle'),
            supportDescription: req.__('supportDescription'),
            pixelInstruction: req.__('pixelInstruction'),
            donateButtonText: req.__('donateButtonText'),
            howItWorks: req.__('howItWorks'),
            howItWorksDescription: req.__('howItWorksDescription'),
            contactTitle: req.__('contactTitle'),
            contactDescription: req.__('contactDescription'),
            contactLinkText: req.__('contactLinkText'),
            modalBuyPixels: req.__('modalBuyPixels'),          // added
            modalSelectPixels: req.__('modalSelectPixels'),    // added
            modalBuyNow: req.__('modalBuyNow'),               // added
            slogan : req.__('slogan'),
            adsSupportTitle : req.__('adsSupportTitle'),
            adsSupportDescription : req.__('adsSupportTitle'),
            emailOptional:req.__('emailOptional'),
            noteText:req.__('noteText'),
            noteText2:req.__('noteText2'),
            lang: lang,
            pixels: []
        });
        
    } catch (error) {
        // Handle any potential errors here
        console.error('Error fetching pixels:', error);
        res.status(500).send('Internal Server Error');
    }
});

// Route to mark a pixel as sold and update the cache
router.post('/sell/:x_coord/:y_coord', async (req, res) => {
    const { x_coord, y_coord } = req.params;

    // Update the database to mark the pixel as sold
    await Pixel.update({ sold: true }, { where: { x_coord, y_coord } });

    // Update the cache by removing the sold pixel from the cache
    const cachedPixels = pixelCache.get('pixels');
    const updatedPixels = cachedPixels.filter(pixel => !(pixel.x_coord === x_coord && pixel.y_coord === y_coord));
    pixelCache.set('pixels', updatedPixels);

    res.json({ success: true, message: 'Pixel sold successfully' });
});


module.exports = router;
