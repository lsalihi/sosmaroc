CREATE TABLE pixels (
  x_coord INTEGER NOT NULL,
  y_coord INTEGER NOT NULL,
  owner VARCHAR(255) DEFAULT 'SYSTEM',
  sold BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (x_coord, y_coord)
);

-- Generate rows 
INSERT INTO pixels (x_coord, y_coord)
SELECT x, y
FROM generate_series(1, 1000) as x,
     generate_series(1, 1000) as y;

UPDATE pixels 
SET 
    sold = false,
    owner = NULL, 
    url = NULL, 
    image = NULL,
    color = '#FFFFFF';

-- Randomly update some rows  
UPDATE pixels
SET sold = true
WHERE random() < 0.1;


-- using partition

DO $$
DECLARE
    partition_name TEXT;
    i INTEGER;
BEGIN
    FOR i IN 1..100 LOOP
        partition_name := format('pixels_part_%s', i);
        EXECUTE 'DROP TABLE IF EXISTS ' || partition_name;
    END LOOP;

    EXECUTE 'DROP TABLE IF EXISTS pixels';
END $$;

-- Create the main partitioned table
CREATE TABLE pixels (
    x_coord INTEGER NOT NULL,
    y_coord INTEGER NOT NULL,
    owner VARCHAR(255) DEFAULT 'SYSTEM',
    sold BOOLEAN DEFAULT FALSE
) PARTITION BY RANGE (x_coord);
-- Dynamically create 100 partitions
DO $$
DECLARE
    start_range INTEGER := 1;
    end_range INTEGER := 11;  -- Start the first partition from 1 to 10
    i INTEGER;
BEGIN
    FOR i IN 1..100 LOOP
        EXECUTE format('
            CREATE TABLE IF NOT EXISTS pixels_part_%s PARTITION OF pixels 
            FOR VALUES FROM (%s) TO (%s)', 
            i, start_range, end_range
        );
        start_range := end_range;      -- Move to the next value for starting the range
        end_range := end_range + 10;   -- Add 10 for the next partition's ending value
    END LOOP;
END $$;

/*
DO $$
DECLARE
    partition_start INTEGER := 1;
    partition_end INTEGER := 10;  -- Each partition handles 10 x-coordinates.
    partition_name TEXT;
BEGIN
    FOR i IN 1..100 LOOP
        partition_name := 'pixels_part_' || i;

        EXECUTE format('
            INSERT INTO %I (x_coord, y_coord)
            SELECT x, y
            FROM generate_series(%s, %s) as x,
                 generate_series(1, 1000) as y  -- y-coordinates range from 1 to 1000 for every x-coordinate
            ', 
            partition_name, partition_start, partition_end
        );

        partition_start := partition_end + 1;  -- Move the start to the next x-coordinate
        partition_end := partition_end + 10;   -- Handle the next 10 x-coordinates
    END LOOP;
END $$; */

select count(*) from pixels_part_15