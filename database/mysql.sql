-- Creating the main partitioned table
CREATE TABLE pixels (
    x_coord INT NOT NULL,
    y_coord INT NOT NULL,
    owner VARCHAR(255) DEFAULT 'SYSTEM',
    sold BOOLEAN DEFAULT FALSE
) ENGINE=InnoDB
PARTITION BY RANGE (x_coord) (
    PARTITION p0 VALUES LESS THAN (0)  -- Dummy partition, will be removed later
);

-- Delimiter change to allow stored procedure definition
DELIMITER //

-- Creating a stored procedure to generate the partitions
CREATE PROCEDURE CreatePartitions()
BEGIN
    DECLARE start_range INT DEFAULT 1;
    DECLARE end_range INT DEFAULT 11;  -- Start the first partition from 1 to 10
    DECLARE i INT DEFAULT 1;

    -- Removing the dummy partition
    ALTER TABLE pixels DROP PARTITION p0;

    WHILE i <= 100 DO
        SET @sql = CONCAT(
            'ALTER TABLE pixels ADD PARTITION (PARTITION p', i, 
            ' VALUES LESS THAN (', end_range, '))'
        );

        -- Executing the dynamic SQL
        PREPARE stmt FROM @sql;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;

        SET start_range = end_range;
        SET end_range = end_range + 10;
        SET i = i + 1;
    END WHILE;
END //

-- Changing the delimiter back to ;
DELIMITER ;

-- Calling the stored procedure to create the partitions
CALL CreatePartitions();

-- Cleaning up by dropping the procedure
DROP PROCEDURE CreatePartitions;
