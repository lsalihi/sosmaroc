const stream = new EventSource('/api/pixels');

function handlePixels(pixels) {
    pixels.forEach(pixel => {
        const div = document.createElement('div');
        div.classList.add('pixel-clear');
        div.style.left = pixel.x_coord + 'px';
        div.style.top = pixel.y_coord + 'px';
        document.querySelector('.flag-container').appendChild(div);
    });
}

stream.onmessage = event => {
    try {
        const pixels = JSON.parse(event.data);
        // Render pixels 
        handlePixels(pixels);
    } catch (error) {
        // Log the error and continue
        console.error('Error decompressing data:', error);
    }
};

// Error handler
stream.onerror = err => {
    // Handle errors
    console.error(err);
};

// Reconnection handler
stream.onopen = () => {
    // Log reconnect
    console.log('Reconnected to stream');
}