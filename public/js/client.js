document.addEventListener('DOMContentLoaded', function() {
    const modal = document.getElementById('checkoutModal');
    const flagContainer = document.querySelector('.flag-container'); // reference to flag container
    const donateButton = document.getElementById('donateButton'); // reference to donate button
    const closeBtn = document.querySelector('.close');
    const pixelCountInput = document.getElementById('pixelCount');
    const totalAmountSpan = document.getElementById('totalAmount');
    const buyNowBtn = document.getElementById('buyNow');

    // Open the modal on flag-container click
    flagContainer.addEventListener('click', function() {
        modal.style.display = "block";
    });

    // Open the modal on donateButton click
    donateButton.addEventListener('click', function() {
        modal.style.display = "block";
    });

    // Close the modal on close button click
    closeBtn.addEventListener('click', function() {
        modal.style.display = "none";
    });

    // Update the total amount whenever the pixel count is changed
    pixelCountInput.addEventListener('input', function() {
        const count = parseInt(pixelCountInput.value) || 0;
        totalAmountSpan.textContent = count; // this will display the number of pixels, not the dollar amount.
    });

    // Handle the buy button click
    buyNowBtn.addEventListener('click', function() {
        const count = parseInt(pixelCountInput.value) || 0;
        if (count > 0) {
            // Here, you'd have your code to randomly assign and "buy" pixels. 
            // You would likely need a function that fetches a list of all unsold pixels and selects a number of them randomly.

            modal.style.display = "none"; // close the modal once the purchase is initiated
        } else {
            alert('Please select a valid number of pixels.');
        }
    });
});


$(document).ready(function() {
    let clicks = 0;

    // Lorsque l'utilisateur clique sur l'unité AdSense.
    $(".adsense-unit").click(function() {
        clicks++; // Incrémenter le compteur de clics.

        // Mettre à jour l'affichage du compteur.
        $("#clickCounter span").text(clicks);

        // Vérifiez si le nombre requis de clics a été atteint.
        if (clicks >= 10) {
            // Attribuez un pixel ici.

            // Réinitialisez le compteur de clics.
            clicks = 0;
            $("#clickCounter span").text(clicks);
        }
    });
});
