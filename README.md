pixel-donations/
|-- node_modules/         # Modules and libraries installed via npm
|-- views/                # EJS templates and views
|   |-- partials/         # Reusable components (like header, footer, etc.)
|   |   |-- header.ejs
|   |   |-- footer.ejs
|   |-- index.ejs         # Main webpage
|-- public/               # Static files
|   |-- css/
|   |   |-- styles.css    # General styling for the website
|   |-- js/
|   |   |-- script.js     # Frontend scripts for pixel selection, etc.
|   |-- images/
|       |-- flag.jpg      # Image of the Moroccan flag
|-- routes/               # Express routes (optional separation for clean structure)
|   |-- index.js          # Main routes
|   |-- payment.js        # Routes related to payment processing
|-- models/               # Data models (if integrating with a database)
|   |-- pixel.js          # Model for each pixel
|-- .env                  # Environment variables (e.g., API keys, database URLs, etc.)
|-- package.json          # Project metadata and dependencies
|-- server.js             # Main entry point of the app, where the Express app is defined


    "title": "Let's Rebuild Morocco Together",